package serie1.exercice2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class SaveCommune {
	
	@SuppressWarnings("resource")
	public static List<Commune> lireDocument()
	{
		List<Commune> liste= new ArrayList<Commune>();
		int count = 0, count2=0, count3=0, count4=0;
		String ligne=null;
		String[] chaine;
		String var;
		try{
			File file = new File("laposte_hexasmal.csv");
			FileReader reader= new FileReader(file); 

			LineNumberReader lnr= new LineNumberReader(reader);
			
			try {
				
				ligne= lnr.readLine();
				
				while(ligne!=null){		
					if(lnr.getLineNumber()!=1){
						Commune commune= new Commune();
						
						chaine=ligne.split(";");
						
						commune.setCodeCommune(chaine[0]);
						commune.setNomCommune(chaine[1]);
						commune.setCodePostale(chaine[2]);
						commune.setLibelleAcheminement(chaine[3]);
						
						if(chaine.length==5){

							commune.setLigne5(chaine[4]);
							count2++;
							if(!chaine[1].equals(chaine[4])){
								count3++;
								
							}
						}
						
						var=chaine[0].substring(0, 2);
						
						if(var.equals("72")){
							
							count4++;
						}
						
						count++;
						
						
						liste.add(commune);
						
						
					}
					ligne= lnr.readLine();
					
				}
				
				System.out.println("Le nombre de ligne est: "+count);
				System.out.println("Le nombre de communes qui ne possede ligne5: "+count2);
				System.out.println("Le nombre de commune dont le nom est different du libelle: "+count3);
				System.out.println("Le nombre de communes dans le 72: "+count4);
				
				
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		return liste;
		
	}
	
	
	
	public static boolean insersionBase(List<Commune> liste){
		
		
		if(liste!=null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
			EntityManager em = emf.createEntityManager() ;
		    
			
			
			for(int i=0; i<liste.size(); i++){
					em.getTransaction().begin();
					em.persist(liste.get(i));
					em.getTransaction().commit();
				
			}
				
		    
			return true;
		}
	
			return false;	
		}

}