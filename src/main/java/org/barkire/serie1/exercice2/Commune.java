package serie1.exercice2;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import serie1.exercice3_4_5.Departement;
import serie1.exercice3_4_5.Maire;

@Entity
public class Commune implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
	
	@Column(name="Code_commune", length=10)
	private String codeCommune;
	@Column(name="Nom_commune", length=50)
	private String NomCommune;
	@Column(name="Code_postale", length=10)
	private String codePostale;
	@Column(name="Libelle_acheminement", length=100)
	private String libelleAcheminement;
	@Column(name="Ligne_5", length=50)
	private String ligne5;
	
	@OneToOne
	private Maire maire;
	@ManyToOne
	private Departement departement;

	@Override
	public String toString() {
		return "Commune [codeCommune=" + codeCommune + ", NomCommune=" + NomCommune + ", codePostale=" + codePostale
				+ ", codeAcheminement=" + libelleAcheminement + ", ligne5=" + ligne5 +  ", Maire=" + maire.getNom() + ", Département=" + departement.getNom() +"]";
	}
	

	public String getCodeCommune() {
		return codeCommune;
	}


	public void setCodeCommune(String codeCommune) {
		this.codeCommune = codeCommune;
	}


	public String getNomCommune() {
		return NomCommune;
	}


	public void setNomCommune(String nomCommune) {
		NomCommune = nomCommune;
	}


	public String getCodePostale() {
		return codePostale;
	}


	public void setCodePostale(String codePostale) {
		this.codePostale = codePostale;
	}


	public String getLibelleAcheminement() {
		return libelleAcheminement;
	}


	public void setLibelleAcheminement(String libelleAcheminement) {
		this.libelleAcheminement = libelleAcheminement;
	}


	public String getLigne5() {
		return ligne5;
	}


	public void setLigne5(String ligne5) {
		this.ligne5 = ligne5;
	}



	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Maire getMaire() {
		return maire;
	}


	public void setMaire(Maire maire) {
		this.maire = maire;
	}


	public Departement getDepartement() {
		return departement;
	}


	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((NomCommune == null) ? 0 : NomCommune.hashCode());
		result = prime * result + ((libelleAcheminement == null) ? 0 : libelleAcheminement.hashCode());
		result = prime * result + ((codeCommune == null) ? 0 : codeCommune.hashCode());
		result = prime * result + ((codePostale == null) ? 0 : codePostale.hashCode());
		result = prime * result + ((ligne5 == null) ? 0 : ligne5.hashCode());
		result = prime * result + ((maire == null) ? 0 : maire.hashCode());
		result = prime * result + ((departement == null) ? 0 : departement.hashCode());
		
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commune other = (Commune) obj;
		if (NomCommune == null) {
			if (other.NomCommune != null)
				return false;
		} else if (!NomCommune.equals(other.NomCommune))
			return false;
		if (libelleAcheminement == null) {
			if (other.libelleAcheminement != null)
				return false;
		} else if (!libelleAcheminement.equals(other.libelleAcheminement))
			return false;
		if (codeCommune == null) {
			if (other.codeCommune != null)
				return false;
		} else if (!codeCommune.equals(other.codeCommune))
			return false;
		if (codePostale == null) {
			if (other.codePostale != null)
				return false;
		} else if (!codePostale.equals(other.codePostale))
			return false;
		if (ligne5 == null) {
			if (other.ligne5 != null)
				return false;
		} else if (!ligne5.equals(other.ligne5))
			return false;
		if (maire == null) {
			if (other.maire != null)
				return false;
		} else if (!maire.equals(other.maire))
			return false;
		if (departement == null) {
			if (other.departement != null)
				return false;
		} else if (!departement.equals(other.departement))
			return false;
		return true;
	}
}
