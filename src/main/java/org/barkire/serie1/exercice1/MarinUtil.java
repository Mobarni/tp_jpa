package serie1.exercice1;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MarinUtil {
	static EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
    static  EntityManager em = emf.createEntityManager() ;
    
	public static long create(Marin marin){        
		em.getTransaction().begin();
        em.persist(marin);
        em.getTransaction().commit();
		return marin.getId();
	}
	
	public static Marin find(long id){
		Marin m= new Marin();
		
        em.getTransaction().begin();
        m= em.find(Marin.class, id);
        em.getTransaction().commit();
        
		return m;
	}
	
		public static boolean delete(long id){
		
        em.getTransaction().begin();
        
        if(em.find(Marin.class, id)==null)
        	return false;
        em.remove(em.find(Marin.class, id));
        em.getTransaction().commit();
        return true;
	}
}
