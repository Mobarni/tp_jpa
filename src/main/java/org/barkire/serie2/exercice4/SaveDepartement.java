package org.barkire.serie2.exercice4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SaveDepartement {

	@SuppressWarnings("resource")
	public static Set<Departement> lireDocument(List<org.barkire.serie1.exercice2.Commune> liste2) throws ParseException
	{
		Set<Departement> liste= new HashSet<Departement>();
		String ligne=null;
		int count=0;
		try{
			File file = new File("maires-25-04-2014.csv");
			FileReader reader= new FileReader(file); 

			LineNumberReader lnr= new LineNumberReader(reader);
			
			try {
				
				
				ligne= lnr.readLine();
				
				String[] chaine;
				while(ligne!=null){		

					chaine=ligne.split(";");
					if(lnr.getLineNumber()>4&&chaine.length==11){

						Departement departement= new Departement();
						for(int i=0; i<liste2.size();i++){
							// Afin d'avoir le code insee du fichier commune
							//j'ai concatené le code departement et le code commune du document maire
							if(liste2.get(i).getCodeCommune().equals(chaine[0]+chaine[2])){
								departement.setNom(chaine[1]);
								departement.setCodeDepartement(chaine[0]);
	
								liste.add(departement);
						
						count++;
							}
					}
				}
				ligne= lnr.readLine();
					
					
				
				}
				
				System.out.println(count);
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		return liste;
		
	}
	
	public static boolean insersionBase(Set<Departement> liste){
		
		if(liste!=null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
			EntityManager em = emf.createEntityManager() ;
		    
			
			
			for (Departement d : liste) {
				em.getTransaction().begin();
				em.persist(d);
				em.getTransaction().commit();
		
				
					
			}
			
		    
			return true;
		}
	
			return false;	
		}
}
