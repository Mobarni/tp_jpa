package org.barkire.serie2.exercice4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Departement implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3906368861257484212L;
	
	 @Id
	 @GeneratedValue(strategy= GenerationType.TABLE)
	
	 @Column(name="Nom_departement", length=100)
	private String nom;
	
	@Column(name="Code_departement", length=50)
	private String codeDepartement;
	

	@OneToMany(mappedBy="departement")
	 private List<Commune> communes= new ArrayList<Commune>();
	 
	
   
		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getCodeDepartement() {
			return codeDepartement;
		}

		public void setCodeDepartement(String codeDepartement) {
			this.codeDepartement = codeDepartement;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((codeDepartement == null) ? 0 : codeDepartement.hashCode());
			result = prime * result + ((nom == null) ? 0 : nom.hashCode());
			result = prime * result + ((communes == null) ? 0 : communes.hashCode());
			
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Departement other = (Departement) obj;
			if (codeDepartement == null) {
				if (other.codeDepartement != null)
					return false;
			} else if (!codeDepartement.equals(other.codeDepartement))
				return false;
			if (nom == null) {
				if (other.nom != null)
					return false;
			} else if (!nom.equals(other.nom))
				return false;
			if (communes == null) {
				if (other.communes != null)
					return false;
			} else if (!communes.equals(other.communes))
				return false;
			return true;
		}

			@Override
		public String toString() {
			return "Departement [nom=" + nom + ", codeDepartement=" + codeDepartement + "Communes="+communes+"]";
		}

}
