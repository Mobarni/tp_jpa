package org.barkire.serie2.exercice4;


public enum Civilite {

	Monsieur ("M"),
	Madame ("Mme");
	
	private String abreviation ; 
   
	private Civilite(String abreviation) {  
         this.abreviation = abreviation ;  
    }  
      
     public String getAbreviation() {  
         return  this.abreviation ;  
    }  
}