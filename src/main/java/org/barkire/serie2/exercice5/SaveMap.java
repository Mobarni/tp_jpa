package org.barkire.serie2.exercice5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.barkire.serie1.exercice2.SaveCommune;
import org.barkire.serie2.exercice3.Departement;
import org.barkire.serie2.exercice3.SaveDepartement;

public class SaveMap {

	@SuppressWarnings({ "resource", "null", "finally" })
	public static Map<String, List<org.barkire.serie1.exercice2.Commune>> lireDocument(List<org.barkire.serie1.exercice2.Commune> liste2) throws ParseException
	{
		List<org.barkire.serie1.exercice2.Commune> communes= new ArrayList<org.barkire.serie1.exercice2.Commune>();
		Map<String, List<org.barkire.serie1.exercice2.Commune>> liste = null;
		
		Set<Departement> listeDepartements;
		try {
			listeDepartements = SaveDepartement.lireDocument(liste2);
			for(Departement d: listeDepartements ){
				for(int i=0; i<liste2.size();i++){
					// Afin d'avoir le code insee du fichier commune
					//j'ai concatené le code departement et le code commune du document maire
					if(liste2.get(i).getCodeCommune().equals(d.getCodeDepartement()+liste2.get(i).getCodeCommune())){
						
						communes.add(liste2.get(i));
				
					}
				
				liste.put(d.getCodeDepartement(), communes);
		
			
				}
			
			}
		}
			catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	
		
		return liste;
	}
	
	public static boolean insersionBase(Set<Departement> liste){
		
		if(liste!=null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
			EntityManager em = emf.createEntityManager() ;
		    
			
			
			for (Departement d : liste) {
				em.getTransaction().begin();
				em.persist(d);
				em.getTransaction().commit();
		
				
					
			}
			
		    
			return true;
		}
	
			return false;	
		}
}
