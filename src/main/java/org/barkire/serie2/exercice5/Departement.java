package org.barkire.serie2.exercice5;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;


@Entity
public class Departement implements Serializable{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3906368861257484212L;
	
	 @Id
	 @GeneratedValue(strategy= GenerationType.TABLE)
	 private long idDepartement;
	 
	 /**
	 * @return the idDepartement
	 */
	public long getIdDepartement() {
		return idDepartement;
	}

	/**
	 * @param idDepartement the idDepartement to set
	 */
	public void setIdDepartement(long idDepartement) {
		this.idDepartement = idDepartement;
	}

	@Column(name="Nom_departement", length=100)
	private String nom;
	
	@Column(name="Code_departement", length=50)
	private String codeDepartement;
   
	 
	@OneToMany(mappedBy="departement")
    @MapKeyColumn(name="Code_departement")
    private Map<String, List<org.barkire.serie1.exercice2.Commune>> communesByCodeDep ;
   
		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getCodeDepartement() {
			return codeDepartement;
		}

		public void setCodeDepartement(String codeDepartement) {
			this.codeDepartement = codeDepartement;
		}

		

}
