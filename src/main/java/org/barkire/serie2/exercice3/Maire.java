package serie1.exercice3_4_5;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import serie1.exercice2.Commune;

@Entity
public class Maire implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3316895663526032524L;

	@Id
	@GeneratedValue(strategy= GenerationType.TABLE)
	private long id;
	
	@Column(name="Nom", length=50)
	private String nom;
	@Column(name="Prenom", length=50)
	private String prenom;
	@Column(name="Civilite", length=15)
	@Enumerated(EnumType.STRING)
	private Civilite civilite;
	@Column(name="Date_de_naissance", length=15)
	@Temporal(TemporalType.DATE)
	private Date dateDeNaissance;
	
	private Commune commune;
	

	public Commune getCommune() {
		return commune;
	}
	public void setCommune(Commune commune) {
		this.commune = commune;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Civilite getCivilite() {
		return civilite;
	}
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}
	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}
	public void setDateDeNaissance(Date date) {
		this.dateDeNaissance = date;
	}
	@Override
	public String toString() {
		return "Maire [nom=" + nom + ", prenom=" + prenom + ", civilite=" + civilite + ", dateDeNaissance="
				+ dateDeNaissance + "Commune="+commune.getNomCommune() +"]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((civilite == null) ? 0 : civilite.hashCode());
		result = prime * result + ((dateDeNaissance == null) ? 0 : dateDeNaissance.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + ((commune == null) ? 0 : commune.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Maire other = (Maire) obj;
		if (civilite != other.civilite)
			return false;
		if (dateDeNaissance == null) {
			if (other.dateDeNaissance != null)
				return false;
		} else if (!dateDeNaissance.equals(other.dateDeNaissance))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (commune == null) {
			if (other.commune != null)
				return false;
		} else if (!commune.equals(other.commune))
			return false;
		return true;
	}
	
	
	
}
