package org.barkire.serie2.exercice3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SaveDepartement {

	@SuppressWarnings("resource")
	public static List<Departement> lireDocument() throws ParseException
	{
		List<Departement> liste= new ArrayList<Departement>();
		String ligne=null;
		int count=0;
		try{
			File file = new File("maires-25-04-2014.csv");
			FileReader reader= new FileReader(file); 

			LineNumberReader lnr= new LineNumberReader(reader);
			
			try {
				
				
				ligne= lnr.readLine();
				
				String[] chaine;
				while(ligne!=null){		

					chaine=ligne.split(";");
					if(lnr.getLineNumber()>4&&chaine.length==11){

						Departement departement= new Departement();
						
						departement.setNom(chaine[1]);
						departement.setCodeDepartement(chaine[0]);

						liste.add(departement);
						
						count++;
					}
					ligne= lnr.readLine();
					
					
				
				}
				
				System.out.println(count);
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		return liste;
		
	}
	
	public static boolean insersionBase(List<Departement> liste){
		
		if(liste!=null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
			EntityManager em = emf.createEntityManager() ;
		    
			
			
			for(int i=0; i<liste.size(); i++){
					em.getTransaction().begin();
					em.persist(liste.get(i));
					em.getTransaction().commit();
			
			}
			
		    
			return true;
		}
	
			return false;	
		}
}
