package serie1.exercice3_4_5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



public class SaveMaire {

	
	@SuppressWarnings("resource")
	public static List<Maire> lireDocument() throws ParseException
	{
		List<Maire> liste= new ArrayList<Maire>();
		String ligne=null;
		int count=0;
		try{
			File file = new File("maires-25-04-2014.csv");
			FileReader reader= new FileReader(file); 

			LineNumberReader lnr= new LineNumberReader(reader);
			
			try {
				
				
				ligne= lnr.readLine();
				
				SimpleDateFormat date= new SimpleDateFormat("dd/mm/yyyy");
				String[] chaine;
				while(ligne!=null){		

					chaine=ligne.split(";");
					if(lnr.getLineNumber()>4&&chaine.length==11){
						Civilite civilite = null;
						Maire maire= new Maire();
						
						
						System.out.println(chaine[8]);
						if(chaine[7].equals("M")){
							civilite= Civilite.Monsieur;
						}
						if(chaine[7].equals("Mme")){
							civilite= Civilite.Madame;
						}
						if(chaine[7].equals("Mlle")){
							civilite= Civilite.Madame;
						}
						maire.setNom(chaine[5]);
						maire.setPrenom(chaine[6]);
						maire.setCivilite(civilite);

						maire.setDateDeNaissance(date.parse(chaine[8]));
						
						liste.add(maire);
						
						count++;
					}
					ligne= lnr.readLine();
					
					
				
				}
				
				System.out.println(count);
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		
		return liste;
		
	}
	
	public static boolean insersionBase(List<Maire> liste){
		
		if(liste!=null){ 
			EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-test") ;
			EntityManager em = emf.createEntityManager() ;
		    
			
			
			for(int i=0; i<liste.size(); i++){
					em.getTransaction().begin();
					em.persist(liste.get(i));
					em.getTransaction().commit();
			
			}
			
		    
			return true;
		}
	
			return false;	
		}
}
